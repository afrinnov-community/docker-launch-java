FROM openjdk:8-jdk-alpine
ARG JAR_FILE=target/*.war
COPY ${JAR_FILE} app.war
EXPOSE 58080
ENV SPRING_OUTPUT_ANSI_ENABLED=ALWAYS \
    SPRING_PROFILES_ACTIVE=dev \
    URL="jdbc:mysql://byommysql:3306/byomserver?useSSL=false&allowPublicKeyRetrieval=true" \
    PASSWORD=afrinnov \
	USERNAME=root

# ENTRYPOINT ["java","-jar","/app.jar"]
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom" ,"-jar","/app.war","--spring.datasource.username=${USERNAME}","--spring.datasource.password=${PASSWORD}", "spring.datasource.url=${URL}", "spring.profiles.active=${SPRING_PROFILES_ACTIVE}"]


